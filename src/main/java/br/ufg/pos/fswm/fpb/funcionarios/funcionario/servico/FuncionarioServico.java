package br.ufg.pos.fswm.fpb.funcionarios.funcionario.servico;

import br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade.Funcionario;
import br.ufg.pos.fswm.fpb.funcionarios.funcionario.repositorio.FuncionarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
@Service
public class FuncionarioServico {

    @Autowired
    private FuncionarioRepositorio funcionarioRepositorio;

    public void cadastrarNovo(Funcionario funcionario) {
        System.out.println("Cadastrando novo funcionario");
        funcionarioRepositorio.save(funcionario);
        System.out.println("Funcionario salvo com sucesso");
    }

    public Funcionario encontrarPorId(Long id) {
        System.out.println("Buscando funcionario por ID");
        return funcionarioRepositorio.findOne(id);
    }

    public List<Funcionario> buscarTodos() {
        System.out.println("Buscando todos os funcionarios");
        return funcionarioRepositorio.findAll();
    }

    public void excluir(Long id) {
        System.out.println("Excluindo funcionario");
        funcionarioRepositorio.delete(id);
    }
}
