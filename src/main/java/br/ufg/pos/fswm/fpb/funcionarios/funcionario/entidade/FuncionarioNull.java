package br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
public class FuncionarioNull extends Funcionario {

    public FuncionarioNull() {
        super(0L, "Sem Nome", "00000000000", "sem email", "00 0000-0000", null);
    }
}
