package br.ufg.pos.fswm.fpb.funcionarios.funcionario.repositorio;

import br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
@Repository
public interface FuncionarioRepositorio extends JpaRepository<Funcionario, Long>{
}
