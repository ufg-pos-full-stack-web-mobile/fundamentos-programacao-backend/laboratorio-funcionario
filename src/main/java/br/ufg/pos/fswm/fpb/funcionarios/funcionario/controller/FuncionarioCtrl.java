package br.ufg.pos.fswm.fpb.funcionarios.funcionario.controller;

import br.ufg.pos.fswm.fpb.funcionarios.funcionario.controller.dto.FuncionarioDTO;
import br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade.Funcionario;
import br.ufg.pos.fswm.fpb.funcionarios.funcionario.servico.FuncionarioServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
@RestController
@RequestMapping("/funcionarios")
public class FuncionarioCtrl {

    @Autowired
    private FuncionarioServico funcionarioServico;

    @PutMapping("/novo")
    public void adicionarNovo(@RequestBody @Valid FuncionarioDTO dto,  BindingResult result) {
        System.out.println("Recebendo um novo dto de funcionario");
        final Funcionario funcionario = FuncionarioDTO.FuncionarioDTOTransformer.criarEntidade(dto);
        funcionarioServico.cadastrarNovo(funcionario);
    }

    @GetMapping("/{id}")
    public @ResponseBody FuncionarioDTO buscarPorId(@PathVariable("id") Long id) {
        System.out.println("Buscando funcionario com ID " + id);
        final Funcionario funcionario = funcionarioServico.encontrarPorId(id);
        return FuncionarioDTO.FuncionarioDTOTransformer.criarDto(funcionario);
    }

    @GetMapping
    public @ResponseBody List<FuncionarioDTO> buscarTodos() {
        System.out.println("Buscando todos os funcionarios");
        final List<Funcionario> funcionarios = funcionarioServico.buscarTodos();
        final List<FuncionarioDTO> dtos = new ArrayList<FuncionarioDTO>();
        for (Funcionario funcionario : funcionarios) {
            dtos.add(FuncionarioDTO.FuncionarioDTOTransformer.criarDto(funcionario));
        }
        return dtos;
    }

    @DeleteMapping("/{id}")
    public void excluirPorId(@PathVariable("id") Long id) {
        System.out.println("Excluindo funcionario com id " + id);
        funcionarioServico.excluir(id);
    }
}
