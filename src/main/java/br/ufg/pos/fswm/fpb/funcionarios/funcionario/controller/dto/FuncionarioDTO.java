package br.ufg.pos.fswm.fpb.funcionarios.funcionario.controller.dto;

import br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade.Funcionario;
import br.ufg.pos.fswm.fpb.funcionarios.funcionario.entidade.FuncionarioNull;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 24/06/17.
 */
public class FuncionarioDTO {

    @Size(max = 30)
    private String nome;

    @Size(min = 11, max = 11)
    private String cpf;

    @Size(max = 35)
    private String email;

    private Date nascimento;

    @Size(max = 20)
    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "FuncionarioDTO{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", nascimento=" + nascimento +
                ", telefone='" + telefone + '\'' +
                '}';
    }

    public static class FuncionarioDTOTransformer {
        private FuncionarioDTOTransformer() {
            // util class
        }

        public static Funcionario criarEntidade(FuncionarioDTO dto) {
            Funcionario funcionario = new Funcionario();
            funcionario.setNome(dto.getNome());
            funcionario.setEmail(dto.getEmail());
            funcionario.setCpf(dto.getCpf());
            funcionario.setNascimento(dto.getNascimento());
            funcionario.setTelefone(dto.getTelefone());
            return funcionario;
        }

        public static FuncionarioDTO criarDto(Funcionario funcionario) {
            if (funcionario == null) {
                funcionario = new FuncionarioNull();
            }
            final FuncionarioDTO dto = new FuncionarioDTO();
            dto.setNome(funcionario.getNome());
            dto.setCpf(funcionario.getCpf());
            dto.setEmail(funcionario.getEmail());
            dto.setNascimento(funcionario.getNascimento());
            dto.setTelefone(funcionario.getTelefone());
            return dto;
        }
    }
}
